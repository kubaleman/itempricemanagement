.DEFAULT_GOAL := help

.PHONY: help create build up stop down recreate create fixtures-load bash

COMPOSE_FILE = -f docker-compose.dev.yml
DOCKER_COMPOSE = docker compose ${COMPOSE_FILE}
CONTAINER_EXEC = ${DOCKER_COMPOSE} exec itemprice-management-php
help:
	@awk 'BEGIN {FS = ":.*?## "} /^[a-zA-Z_-]+:.*?## / {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}' $(MAKEFILE_LIST)

create: build up compile fixtures-load ## Create application from scratch
recreate: down create ## Remove application and build it from scratch
build: ## Build local images
	${DOCKER_COMPOSE} build
up: ## Start application
	${DOCKER_COMPOSE} up -d
rebuild: build up ## Build and start application
stop: ## Stop application
	${DOCKER_COMPOSE} stop
down: ## Remove application
	${DOCKER_COMPOSE} down
compile: ## Prepare application codebase
	${CONTAINER_EXEC} composer install
	${CONTAINER_EXEC} bin/console doctrine:migrations:migrate --no-interaction
fixtures-load: ## Load database fixtures (dev and test only)
	${CONTAINER_EXEC} bin/console doctrine:fixtures:load --no-interaction
bash: ## Login to PHP container
	${CONTAINER_EXEC} bash