<?php declare(strict_types=1);

namespace App\Tests\Unit\Item;

use App\Tests\Builders\ItemPriceBuilder;
use Money\Currency;
use Money\Money;
use PHPUnit\Framework\TestCase;
use Source\Item\Domain\Event\ItemPriceChanged;
use Source\Item\Domain\Event\ItemPriceCreated;
use Source\Item\Domain\Exception\ItemDomainException;
use Source\Item\Domain\ItemPrice;
use Source\Item\Shared\ItemId;

final class ItemPriceTest extends TestCase
{
    public function test_should_create_new_item_price_and_record_events(): void
    {
        $itemId = new ItemId();

        $amount = "2500";
        $currency = "PLN";
        $price = new Money($amount, new Currency($currency));

        $itemPrice = ItemPrice::new($itemId, $amount);

        $this->assertEquals($itemId, $itemPrice->getItemId());
        $this->assertEquals($price, $itemPrice->getPrice());

        $events = $itemPrice->getEvents();
        $recordedEvent = $events[0];
        self::assertCount(1, $events);
        self::assertInstanceOf(ItemPriceCreated::class, $recordedEvent);
        self::assertNotEmpty($recordedEvent->payload()['itemPriceId']);
        self::assertEquals($itemId, $recordedEvent->payload()['itemId']);
        self::assertEquals($price->jsonSerialize(), $recordedEvent->payload()['price']);
    }

    public function test_should_not_create_new_item_with_negative_price(): void
    {
        $this->expectException(ItemDomainException::class);

        $this->expectExceptionMessage('item.negative_amount');
        $this->expectExceptionCode(ItemDomainException::DomainBreakCode);

        $amount = "-2500";

        ItemPrice::new(new ItemId(), $amount);
    }

    public function test_should_change_item_price(): void
    {
        $newAmount = "3000";
        $expectedPrice = new Money($newAmount, new Currency("PLN"));

        $itemPrice = ItemPriceBuilder::default("2500")->build();

        $itemPrice->changePrice($newAmount);

        $this->assertEquals($expectedPrice, $itemPrice->getPrice());

        $events = $itemPrice->getEvents();

        self::assertCount(1, $events);
        self::assertInstanceOf(ItemPriceChanged::class, $events[0]);
        $payload = $events[0]->payload();
        self::assertEquals($itemPrice->getItemId(), $payload['itemId']);
        self::assertEquals($expectedPrice->jsonSerialize(), $payload['price']);
        self::assertEquals((string) $itemPrice->getId(), $payload['itemPriceId']);
    }

    public function test_should_not_change_item_price_to_negative(): void
    {
        $this->expectException(ItemDomainException::class);
        $this->expectExceptionMessage('item.negative_amount');
        $this->expectExceptionCode(ItemDomainException::DomainBreakCode);

        $itemPrice = ItemPriceBuilder::default("2500")->build();

        $newAmount = "-3000";

        $itemPrice->changePrice($newAmount);
    }
}
