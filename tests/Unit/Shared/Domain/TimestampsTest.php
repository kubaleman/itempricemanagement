<?php declare(strict_types=1);

namespace App\Tests\Unit\Shared\Domain;

use PHPUnit\Framework\TestCase;

class TimestampsTest extends TestCase
{
    public function test_should_update_time_stamps_to_the_current_time(): void
    {
        $dummyTimestampedEntity = new DummyTimestampedEntity();
        $dummyTimestampedEntity->updatedTimestamps();

        self::assertNotNull($dummyTimestampedEntity->getCreatedAt());
        self::assertNotNull($dummyTimestampedEntity->getUpdatedAt());
        self::assertEquals($dummyTimestampedEntity->getCreatedAt(), $dummyTimestampedEntity->getUpdatedAt());
    }

    public function test_should_update_updated_at_time_stamp_to_the_current_time(): void
    {
        $dummyTimestampedEntity = new DummyTimestampedEntity();
        $dummyTimestampedEntity->updatedTimestamps();

        $updatedAt = $dummyTimestampedEntity->getUpdatedAt();
        $createdAt = $dummyTimestampedEntity->getCreatedAt();
        $dummyTimestampedEntity->updatedTimestamps();

        self::assertEquals($createdAt, $updatedAt);
        self::assertNotEquals($updatedAt, $dummyTimestampedEntity->getUpdatedAt());
    }
}
