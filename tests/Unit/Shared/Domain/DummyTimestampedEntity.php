<?php declare(strict_types=1);

namespace App\Tests\Unit\Shared\Domain;

use Source\Shared\Domain\TimestampsTrait;

final class DummyTimestampedEntity
{
    use TimestampsTrait;
}
