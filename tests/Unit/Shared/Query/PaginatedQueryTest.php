<?php declare(strict_types=1);

namespace App\Tests\Unit\Shared\Query;

use PHPUnit\Framework\TestCase;

final class PaginatedQueryTest extends TestCase
{
    public function test_should_exclude_field_from_sorting(): void
    {
        $expectedSortField = 'createdAt';

        $query = new DummyWithExcludedQueryPagination('ids', 'sortOrder', 1, 10);

        $this->assertEquals($expectedSortField, $query->getSortField());
    }
}
