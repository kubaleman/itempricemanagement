<?php declare(strict_types=1);

namespace App\Tests\Unit\Shared\Query;

use Override;
use Source\Shared\Query\QueryPagination;

final class DummyWithExcludedQueryPagination extends QueryPagination
{
    public function __construct(
        ?string $sortField = null,
        ?string $sortOrder = null,
        ?int $page = null,
        ?int $perPage = null,
    ) {
        parent::__construct($sortField, $sortOrder, $page, $perPage);
    }

    #[Override]
    public function excludedOrderFields(): array
    {
        return ['ids'];
    }
}
