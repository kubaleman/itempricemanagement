<?php declare(strict_types=1);

namespace App\Tests\Builders;

use Money\Money;
use Source\Item\Domain\ItemPrice;
use Source\Item\Domain\ItemPriceId;
use Source\Item\Shared\ItemId;

final readonly class ItemPriceBuilder
{
    private function __construct(
        private ItemPriceId $id,
        private ItemId $itemId,
        private Money $price,
    ) {
    }

    /**
     * @param numeric-string $amount
     */
    public static function default(string $amount = '1000'): self
    {
        return new self(
            new ItemPriceId(),
            new ItemId(),
            Money::PLN($amount)
        );
    }

    public static function create(ItemPriceId $itemPriceId, ItemId $itemId, Money $price): self
    {
        return new self(
            $itemPriceId,
            $itemId,
            $price
        );
    }

    public function build(): ItemPrice
    {
        return new ItemPrice(
            $this->id,
            $this->itemId,
            $this->price,
        );
    }
}
