<?php declare(strict_types=1);

namespace App\Tests\Builders;

use Modules\Identity\ResourceId;
use Source\Item\Domain\ItemDetails;
use Source\Item\Shared\ItemId;

final readonly class ItemDetailsBuilder
{
    private function __construct(
        private ResourceId $id,
        private ItemId $itemId,
        private string $name,
        private string $description,
    ) {
    }

    public static function create(
        ResourceId $id,
        ItemId $itemId,
        string $name,
        string $description
    ): self {
        return new self(
            $id,
            $itemId,
            $name,
            $description
        );
    }

    public function build(): ItemDetails
    {
        return new ItemDetails(
            $this->id,
            $this->itemId,
            $this->name,
            $this->description,
        );
    }
}
