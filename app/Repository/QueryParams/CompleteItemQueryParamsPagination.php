<?php declare(strict_types=1);

namespace App\Repository\QueryParams;

use Override;
use Source\Shared\Assert\Assert;
use Source\Shared\Query\QueryPagination;

final class CompleteItemQueryParamsPagination extends QueryPagination
{
    /**
     * @param array<string>|null $itemIds
     */
    public function __construct(
        public readonly ?array $itemIds = null,
        ?string $sortField = null,
        ?string $sortOrder = null,
        ?int $page = null,
        ?int $perPage = null,
    ) {
        Assert::allUuid($itemIds ?? []);
        parent::__construct($sortField, $sortOrder, $page, $perPage);
    }

    #[Override]
    public function excludedOrderFields(): array
    {
        return ['itemIds'];
    }
}
