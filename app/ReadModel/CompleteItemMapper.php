<?php declare(strict_types=1);

namespace App\ReadModel;

use App\Controller\Api\Admin\Item\Response\CompleteItemResponseDTO;
use App\Repository\QueryParams\CompleteItemQueryParamsPagination;
use Source\Item\Query\ItemDetailsQuery;
use Source\Item\Query\ItemPriceQuery;
use Source\Item\Query\ItemPriceQueryParamsPagination;
use Source\Shared\Respond\Collection;

final readonly class CompleteItemMapper
{
    public function __construct(
        private ItemPriceQuery $itemPriceQuery,
        private ItemDetailsQuery $itemDetailsQuery
    ) {
    }

    public function get(CompleteItemQueryParamsPagination $queryParams): Collection
    {
        $itemPriceQueryParams = $this->getItemPriceQueryParams($queryParams);
        $pricesDtos = $this->itemPriceQuery->get($itemPriceQueryParams);

        $itemsDetails =
            $this->itemDetailsQuery->findByItemIds($itemPriceQueryParams->itemIds);

        $details = [];
        foreach ($itemsDetails as $itemsDetail) {
            $details[(string) $itemsDetail->getItemId()] = $itemsDetail->getDetailsDTO();
        }

        /** @var array<CompleteItemResponseDTO> $itemsPresentable */
        $itemsPresentable = [];
        foreach ($pricesDtos as $priceDTO) {
            $detailsDto = $details[(string) $priceDTO->itemId] ?? null;
            $itemsPresentable[] = CompleteItemResponseDTO::fromDto($priceDTO, $detailsDto);
        }

        return new Collection(
            $itemsPresentable,
            $pricesDtos->total(),
        );
    }

    private function getItemPriceQueryParams(CompleteItemQueryParamsPagination $queryParams): ItemPriceQueryParamsPagination
    {
        return new ItemPriceQueryParamsPagination(
            itemIds: $queryParams->itemIds,
            sortField: $queryParams->sortField,
            sortOrder: $queryParams->sortOrder,
            page: $queryParams->getPage(),
            perPage: $queryParams->getPerPage(),
        );
    }
}
