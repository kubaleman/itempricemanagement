<?php declare(strict_types=1);

namespace App\Controller\Api\Admin\Item\Response;

use Symfony\Component\Uid\Uuid;

final readonly class OrderItemResponseDto
{
    public function __construct(
        public Uuid $orderItemId,
        public Uuid $itemId,
        public string $itemName,
        public string $description,
        public string $itemPrice,
    ) {
    }
}
