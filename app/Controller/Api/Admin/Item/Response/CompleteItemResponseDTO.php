<?php declare(strict_types=1);

namespace App\Controller\Api\Admin\Item\Response;

use Modules\Identity\Uuid;
use Money\Money;
use Source\Item\Domain\DTO\ItemDetailsDTO;
use Source\Item\Query\ItemPriceDTO;
use Source\Shared\Respond\PresentableResponse;

final readonly class CompleteItemResponseDTO implements PresentableResponse
{
    public function __construct(
        public Uuid $itemId,
        public Money $price,
        public ?string $name,
        public ?string $description,
    ) {
    }

    public static function fromDto(ItemPriceDTO $itemPriceDTO, ?ItemDetailsDTO $itemDetails): self
    {
        $name = null;
        $description = null;

        if ($itemDetails !== null) {
            $name = $itemDetails->name;
            $description = $itemDetails->description;
        }

        return new self(
            $itemPriceDTO->itemId,
            $itemPriceDTO->price,
            $name,
            $description,
        );
    }
}
