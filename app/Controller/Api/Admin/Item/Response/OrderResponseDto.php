<?php declare(strict_types=1);

namespace App\Controller\Api\Admin\Item\Response;

use Symfony\Component\Uid\Uuid;

final readonly class OrderResponseDto
{
    public function __construct(
        public Uuid $id,
        //public array $items,
    ) {
    }
}
