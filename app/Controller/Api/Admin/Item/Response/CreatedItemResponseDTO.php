<?php declare(strict_types=1);

namespace App\Controller\Api\Admin\Item\Response;

use Modules\Identity\Uuid;
use OpenApi\Attributes as OA;

final readonly class CreatedItemResponseDTO
{
    public function __construct(
        #[OA\Property(description: 'The id of the created item', type: 'string', format: 'uuid')]
        public Uuid $id,
    ) {
    }
}
