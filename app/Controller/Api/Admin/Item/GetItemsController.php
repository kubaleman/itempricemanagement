<?php declare(strict_types=1);

namespace App\Controller\Api\Admin\Item;

use App\Controller\Api\Admin\Item\Response\CompleteItemResponseDTO;
use App\ReadModel\CompleteItemMapper;
use App\Repository\QueryParams\CompleteItemQueryParamsPagination;
use OpenApi\Attributes as OA;
use Source\Shared\OpenApi\CollectionResponse;
use Source\Shared\Respond\ApiResponder;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Attribute\AsController;
use Symfony\Component\Routing\Attribute\Route;

#[Route(
    path: '/api/items',
    name: 'items_get',
    methods: ['GET'],
)]
#[OA\Get(
    description: 'Get a list of items',
    tags: ['Items'],
    parameters: [
        new OA\QueryParameter(name: 'itemIds', description: "Excluded from sort", schema: new OA\Schema(type: 'array', items: new OA\Items(type: 'string', format: 'uuid'))),
        new OA\QueryParameter(name: 'sortField', schema: new OA\Schema(type: 'string')),
        new OA\QueryParameter(name: 'sortOrder', schema: new OA\Schema(enum: ['asc', 'desc'])),
    ],
)]
#[CollectionResponse(CompleteItemResponseDTO::class)]
#[AsController]
final readonly class GetItemsController
{
    public function __construct(
        private CompleteItemMapper $completeItemMapper,
        private ApiResponder $responder,
    ) {
    }

    public function __invoke(CompleteItemQueryParamsPagination $queryParams): JsonResponse
    {
        $collection = $this->completeItemMapper->get($queryParams);

        return $this->responder->collection($collection);
    }
}
