<?php declare(strict_types=1);

namespace App\Controller\Api\Admin\Item\Request;

use OpenApi\Attributes as OA;
use Symfony\Component\Validator\Constraints as Assert;

final readonly class UpsertItemRequestDTO
{
    public function __construct(
        #[Assert\Length(min: 3, max: 512)]
        public string $name,
        #[Assert\NotNull]
        public string $description,
        /** @var numeric-string */
        #[Assert\Type(type: 'digit')]
        #[OA\Property(description: 'Price in the smallest monetary unit', type: 'string', format: 'numeric-string', example: "2500")]
        public string $price,
    ) {
    }
}
