<?php declare(strict_types=1);

namespace App\Controller\Api\Admin\Item;

use App\Controller\Api\Admin\Item\Request\UpsertItemRequestDTO;
use App\Controller\Api\Admin\Item\Response\CreatedItemResponseDTO;
use Modules\Identity\Uuid;
use Nelmio\ApiDocBundle\Annotation\Model;
use OpenApi\Attributes as OA;
use Source\Item\Application\Command\CreateItemPrice;
use Source\Item\Application\Exception\ItemPriceNegativeException;
use Source\Item\Application\Service\UpsertItemDetailsService;
use Source\Shared\Bus\CommandBus;
use Source\Shared\Respond\ApiResponder;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Attribute\AsController;
use Symfony\Component\HttpKernel\Attribute\MapRequestPayload;
use Symfony\Component\Routing\Attribute\Route;

#[Route(
    path: '/api/items',
    name: 'item_create',
    methods: ['POST'],
)]
#[OA\Post(
    description: 'Create a new item',
    requestBody: new OA\RequestBody(
        content: new OA\JsonContent(ref: new Model(type: UpsertItemRequestDTO::class)),
    ),
    tags: ['Items'],
)]
#[OA\Response(
    response: Response::HTTP_CREATED,
    description: 'Item created successfully',
    content: new OA\JsonContent(ref: new Model(type: CreatedItemResponseDTO::class)),
)]
#[AsController]
final readonly class CreateItemController
{
    public function __construct(
        private CommandBus $commandBus,
        private ApiResponder $responder,
        private UpsertItemDetailsService $createItemDetailsService
    ) {
    }

    public function __invoke(#[MapRequestPayload] UpsertItemRequestDTO $request): JsonResponse
    {
        $itemId = new Uuid();

        $command = new CreateItemPrice(
            $itemId,
            $request->price
        );

        try {
            $this->commandBus->sync($command);
        } catch (ItemPriceNegativeException $e) {
            return $this->responder->error($e);
        }

        $this->createItemDetailsService->create(
            $itemId,
            $request->name,
            $request->description
        );

        return $this->responder->created($itemId);
    }
}
