<?php declare(strict_types=1);

namespace App\Controller\Api\Admin\Item;

use App\Controller\Api\Admin\Item\Request\UpsertItemRequestDTO;
use Modules\Identity\Uuid;
use Nelmio\ApiDocBundle\Annotation\Model;
use OpenApi\Attributes as OA;
use Source\Item\Application\Command\ChangeItemPrice;
use Source\Item\Application\Exception\ItemNotFoundException;
use Source\Item\Application\Exception\ItemPriceNegativeException;
use Source\Item\Application\Service\UpsertItemDetailsService;
use Source\Item\Shared\ItemId;
use Source\Shared\Bus\CommandBus;
use Source\Shared\OpenApi\NoContentResponse;
use Source\Shared\OpenApi\NotFoundResponse;
use Source\Shared\Respond\ApiResponder;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Attribute\AsController;
use Symfony\Component\HttpKernel\Attribute\MapRequestPayload;
use Symfony\Component\Routing\Attribute\Route;

#[Route(
    path: '/api/items/{id}',
    name: 'item_update',
    methods: ['PUT'],
)]
#[OA\Put(
    description: 'Update an item',
    requestBody: new OA\RequestBody(
        content: new OA\JsonContent(ref: new Model(type: UpsertItemRequestDTO::class)),
    ),
    tags: ['Items'],
)]
#[NoContentResponse]
#[NotFoundResponse]
#[AsController]
final readonly class UpdateItemController
{
    public function __construct(
        private CommandBus $commandBus,
        private ApiResponder $responder,
        private UpsertItemDetailsService $detailsService
    ) {
    }

    public function __invoke(Uuid $id, #[MapRequestPayload] UpsertItemRequestDTO $request): JsonResponse
    {
        $itemId = ItemId::fromId($id);

        $command = new ChangeItemPrice(
            $itemId,
            $request->price
        );

        try {
            $this->commandBus->sync($command);
            $this->detailsService->update($itemId, $request->name, $request->description);
        } catch (ItemNotFoundException) {
            return $this->responder->notFound();
        } catch (ItemPriceNegativeException $e) {
            return $this->responder->error($e);
        }

        return $this->responder->noContent();
    }
}
