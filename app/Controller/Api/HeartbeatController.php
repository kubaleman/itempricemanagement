<?php declare(strict_types=1);

namespace App\Controller\Api;

use OpenApi\Attributes as OA;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Attribute\AsController;
use Symfony\Component\Routing\Attribute\Route;

#[Route(path: '/api/heartbeat', name: 'heartbeat', methods: ['GET'])]
#[OA\Get(description: "Heartbeat", tags: ['Common'])]
#[OA\Response(
    response: Response::HTTP_OK,
    description: 'OK',
    content: new OA\JsonContent(description: 'Response', type: 'string', example: [
        'status' => 'OK',
    ]),
)]
#[AsController]
final readonly class HeartbeatController
{
    public function __invoke(): JsonResponse
    {
        return new JsonResponse([
            'status' => 'OK',
        ], Response::HTTP_OK);
    }
}
