<?php declare(strict_types=1);

namespace App\Controller\Security;

use OpenApi\Attributes as OA;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Attribute\AsController;
use Symfony\Component\Routing\Attribute\Route;

#[Route('/api/login/check', name: 'api_login_check', methods: ['POST'])]
#[OA\Post(
    description: 'Grant token for user',
    tags: ['User'],
)]
#[OA\Response(
    response: Response::HTTP_OK,
    description: 'Token granted',
    content: new OA\JsonContent(description: 'The value of the granted user token', type: 'string', example: [
        'token' => 'example_token',
    ]),
)]
#[AsController]
class SecurityController extends AbstractController
{
    public function __invoke(): Response
    {
        return new Response();
    }
}
