<?php declare(strict_types=1);

namespace App\Controller\RequestHandlerController;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;

class HttpNotFoundHandleController extends AbstractController
{
    #[Route('/http/404/handle', name: 'app_http_404_handle')]
    public function http404Handler(): Response
    {
        return $this->render('http_handler/404-error.html.twig', response: new Response('', 404));
    }

    #[Route('/http/404/handle-api', name: 'api_http_404_handle')]
    public function http404ApiHandler(): JsonResponse
    {
        return $this->json([
            'message' => 'Not found',
        ], 404);
    }
}
