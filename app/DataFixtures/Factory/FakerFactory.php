<?php declare(strict_types=1);

namespace App\DataFixtures\Factory;

use Faker\Factory;
use Faker\Generator;

final class FakerFactory
{
    private const string DEFAULT_LOCALE = 'en_US';

    /**
     * @var array<string, Generator>
     */
    private static array $generators = [];

    private static int $seed = 1;

    public static function create(string $locale = self::DEFAULT_LOCALE): Generator
    {
        if (array_key_exists($locale, self::$generators)) {
            return self::$generators[$locale];
        }

        $generator = Factory::create($locale);
        $generator->addProvider(new UuidV4Provider($generator));
        $generator->seed(self::$seed);

        self::$generators[$locale] = $generator;
        self::$seed++;

        return $generator;
    }
}
