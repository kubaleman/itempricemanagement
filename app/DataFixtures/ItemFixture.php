<?php declare(strict_types=1);

namespace App\DataFixtures;

use App\DataFixtures\Factory\FakerFactory;
use App\Tests\Builders\ItemDetailsBuilder;
use App\Tests\Builders\ItemPriceBuilder;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Modules\Identity\ResourceId;
use Money\Money;
use Source\Item\Domain\ItemPriceId;
use Source\Item\Shared\ItemId;

class ItemFixture extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        $faker = FakerFactory::create();

        for ($i = 0; $i < 1500; $i++) {
            $itemId = new ItemId($faker->uuid);
            $rawMoney = $faker->randomNumber(4);
            $money = Money::PLN($rawMoney);

            $item = ItemPriceBuilder::create(
                new ItemPriceId($faker->uuid),
                $itemId,
                $money
            )->build();

            $itemDetails = ItemDetailsBuilder::create(
                new ResourceId(),
                $itemId,
                $faker->name . ' ' . $rawMoney . ' ' . $money->getCurrency()->getCode(),
                $faker->text
            )->build();

            $manager->persist($item);
            $manager->persist($itemDetails);

            if ($i % 100 === 0) {
                $manager->flush();
            }
        }

        $manager->flush();
    }
}
