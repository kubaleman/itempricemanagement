<?php declare(strict_types=1);

namespace App\EventListener;

use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\RouterInterface;

readonly class HttpNotFoundListener
{
    public function __construct(
        private RouterInterface $router
    ) {
    }

    public function onKernelException(ExceptionEvent $event): void
    {
        $exception = $event->getThrowable();

        if ($exception instanceof NotFoundHttpException) {
            $request = $event->getRequest();

            if ($this->isApiRequest($request)) {
                $url = $this->router->generate('api_http_404_handle');
            } else {
                $url = $this->router->generate('app_http_404_handle');
            }

            $response = new RedirectResponse($url);
            $event->setResponse($response);
        }
    }

    private function isApiRequest(Request $request): bool
    {
        return str_starts_with($request->getPathInfo(), '/api/');
    }
}
