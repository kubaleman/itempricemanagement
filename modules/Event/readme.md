### Import configurations to files

Start with add imports to configuration files.

#### doctrine.yml
```yaml
imports:
  - { resource: '../../modules/Event/Configuration/doctrine.yml' }
```

#### messenger.yml
```yaml
imports:
  - { resource: '../../modules/Event/Configuration/messenger.yml' }
```


#### messenger.yml
```yaml
imports:
  - { resource: '../modules/Event/Configuration/services.php' }
```

After that you can create and run migrations for Event Entity.

### How to use

EventDispatcher is a module API facade, it can by sync or async.

```php
public function __construct(
    private EventDispatcher $eventDispatcher,
    ManagerRegistry $registry
) {
    parent::__construct($registry, ServiceCall::class);
}
```
```php
$this->eventDispatcher->dispatchMany($entity->popEvents());
```

Events are sync be default.
You can change it by change configuration in `config/services.yml`

```yaml
services:
  Modules\Event\Domain\EventDispatcher:
    class: Modules\Event\Infrastructure\AsyncEventDispatcher
```

### Custom implementation

If you want to use module with your own implementation, you can create Class like `SyncEventDispatcher` for `EventDispatcherInterface` and change it in `config/services.yml`
