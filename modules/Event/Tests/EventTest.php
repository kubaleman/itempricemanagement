<?php declare(strict_types=1);

namespace Modules\Event\Tests;

use Carbon\CarbonImmutable;
use Modules\Event\Infrastructure\Event;
use Modules\Event\Infrastructure\EventPayload;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Uid\Uuid;

final class EventTest extends TestCase
{
    public function test_should_create_new_event(): void
    {
        $eventId = Uuid::v4();
        $eventType = 'event_type';
        $occurredOn = new CarbonImmutable();
        $correlationId = Uuid::v4();
        $payload = [
            'key' => 'value',
        ];

        $expectedPayload = EventPayload::new([
            'key' => 'value',
        ]);

        $event = Event::new(
            $eventId,
            $eventType,
            $occurredOn,
            $correlationId,
            $payload
        );

        self::assertEquals($eventId, $event->getEventId());
        self::assertEquals($eventType, $event->getEventType());
        self::assertEquals($occurredOn, $event->getOccurredOn());
        self::assertEquals($correlationId, $event->getCorrelationId());
        self::assertEquals($expectedPayload, $event->getPayload());
        self::assertEquals($expectedPayload->getJson(), $event->getPayloadJson());
    }

    public function test_should_create_new_event_with_empty_payload(): void
    {
        $payload = [];

        $expectedPayload = EventPayload::new([]);

        $event = Event::new(
            Uuid::v4(),
            'event_type',
            new CarbonImmutable(),
            Uuid::v4(),
            $payload
        );

        self::assertEquals($expectedPayload, $event->getPayload());
        self::assertEquals($expectedPayload->getJson(), $event->getPayloadJson());
    }
}
