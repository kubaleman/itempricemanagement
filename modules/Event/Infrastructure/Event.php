<?php declare(strict_types=1);

namespace Modules\Event\Infrastructure;

use Carbon\CarbonImmutable;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Types\UuidType;
use Symfony\Component\Uid\Uuid;

#[ORM\Entity]
readonly class Event
{
    public function __construct(
        #[ORM\Id]
        #[ORM\Column(type: UuidType::NAME, unique: true)]
        private Uuid $eventId,
        #[ORM\Column(length: 1024)]
        private string $eventType,
        #[ORM\Column(type: 'datetime_immutable')]
        private CarbonImmutable $occurredOn,
        #[ORM\Column(type: UuidType::NAME, nullable: true)]
        private ?Uuid $correlationId,
        #[ORM\Embedded(class: EventPayload::class)]
        private EventPayload $payload,
        #[ORM\Column(type: 'datetime_immutable')]
        private CarbonImmutable $createdAt,
    ) {
    }

    /**
     * @param array<string, mixed> $payload
     */
    public static function new(
        Uuid $eventId,
        string $eventType,
        CarbonImmutable $occurredOn,
        ?Uuid $correlationId,
        array $payload,
    ): self {
        $payload = EventPayload::new($payload);

        return new self($eventId, $eventType, $occurredOn, $correlationId, $payload, CarbonImmutable::now());
    }

    public function getPayloadJson(): string
    {
        return $this->payload->getJson();
    }

    public function getEventId(): Uuid
    {
        return $this->eventId;
    }

    public function getEventType(): string
    {
        return $this->eventType;
    }

    public function getOccurredOn(): CarbonImmutable
    {
        return $this->occurredOn;
    }

    public function getPayload(): EventPayload
    {
        return $this->payload;
    }

    public function getCorrelationId(): ?Uuid
    {
        return $this->correlationId;
    }

    public function getCreatedAt(): CarbonImmutable
    {
        return $this->createdAt;
    }
}
