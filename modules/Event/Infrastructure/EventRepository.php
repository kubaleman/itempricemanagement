<?php declare(strict_types=1);

namespace Modules\Event\Infrastructure;

interface EventRepository
{
    public function save(Event $event): void;
}
