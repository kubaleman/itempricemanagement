<?php declare(strict_types=1);

namespace Modules\Event\Infrastructure;

use Doctrine\ORM\Mapping as ORM;

#[ORM\Embeddable]
final readonly class EventPayload
{
    public function __construct(
        #[ORM\Column(type: 'json', options: [
            "jsonb" => true,
        ])]
        private string $json,
    ) {
    }

    /**
     * @param array<string, mixed> $payload
     */
    public static function new(array $payload): self
    {
        $json = json_encode($payload);
        if ($json === false) {
            $json = '[]';
            //todo przeniesc ta logike do Fabryki i dodac logowanie w przypadku kiedy false
        }

        return new self($json);
    }

    public function getJson(): string
    {
        return $this->json;
    }
}
