<?php declare(strict_types=1);

namespace Modules\Event\Infrastructure;

use Modules\Event\Application\DispatchOutgoingEvent;
use Modules\Event\Domain\EventDispatcher;
use Modules\Event\Domain\OutgoingEvent;
use Override;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Messenger\Stamp\TransportNamesStamp;
use Symfony\Component\Uid\Uuid;

final readonly class AsyncEventDispatcher implements EventDispatcher
{
    private const TRANSPORT_EVENTS = 'events-async';

    public function __construct(
        private MessageBusInterface $bus,
    ) {
    }

    /**
     * @param array<OutgoingEvent> $events
     */
    public function dispatchMany(array $events): void
    {
        foreach ($events as $event) {
            $this->dispatch($event);
        }
    }

    #[Override]
    public function dispatch(OutgoingEvent $event): void
    {
        $correlationId = $event->correlationId ? Uuid::fromString($event->correlationId) : null;

        $command = new DispatchOutgoingEvent(
            $event->eventId,
            $event->eventType,
            $event->occurredOn,
            $correlationId,
            $event->payload()
        );

        $this->events($command);
    }

    private function events(object $command): void
    {
        $asyncTransportStamp = new TransportNamesStamp([self::TRANSPORT_EVENTS]);

        $stamps = [
            $asyncTransportStamp,
        ];

        $this->bus->dispatch($command, $stamps);
    }
}
