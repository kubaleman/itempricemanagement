<?php declare(strict_types=1);

namespace Modules\Event\Application;

use Carbon\CarbonImmutable;
use Symfony\Component\Uid\Uuid;

final readonly class DispatchOutgoingEvent
{
    public function __construct(
        public Uuid $eventId,
        public string $eventType,
        public CarbonImmutable $occurredOn,
        public ?Uuid $correlationId,
        /** @var array<string, mixed> $payload */
        public array $payload
    ) {
    }
}
