<?php declare(strict_types=1);

namespace Modules\Event\Application;

use Modules\Event\Infrastructure\Event;
use Modules\Event\Infrastructure\EventRepository;
use Symfony\Component\Messenger\Attribute\AsMessageHandler;

#[AsMessageHandler]
final readonly class DispatchOutgoingEventHandler
{
    public function __construct(
        private EventRepository $eventRepository
    ) {
    }

    public function __invoke(DispatchOutgoingEvent $command): void
    {
        $event = Event::new(
            $command->eventId,
            $command->eventType,
            $command->occurredOn,
            $command->correlationId,
            $command->payload
        );

        $this->eventRepository->save($event);
    }
}
