<?php declare(strict_types=1);

use Modules\Event\Domain\EventDispatcher;
use Modules\Event\Infrastructure\SyncEventDispatcher;
use Symfony\Component\DependencyInjection\Loader\Configurator\ContainerConfigurator;

return static function (ContainerConfigurator $configurator): void {
    $services = $configurator->services()
        ->defaults()
        ->autowire()
        ->autoconfigure();

    $services->set(EventDispatcher::class)
        ->class(SyncEventDispatcher::class);

    $services->load('Modules\\Event\\', '../*')
        ->exclude('services.php');
};
