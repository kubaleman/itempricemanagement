<?php declare(strict_types=1);

namespace Modules\Event\Domain;

interface EventDispatcher
{
    public function dispatch(OutgoingEvent $event): void;

    /**
     * @param array<OutgoingEvent> $events
     */
    public function dispatchMany(array $events): void;
}
