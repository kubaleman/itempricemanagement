<?php declare(strict_types=1);

namespace Modules\Event\Domain;

use Carbon\CarbonImmutable;
use Source\Shared\Assert\Assert;
use Symfony\Component\Uid\Uuid;

abstract readonly class OutgoingEvent
{
    public Uuid $eventId;

    public string $eventType;

    public CarbonImmutable $occurredOn;

    public function __construct(
        public ?string $correlationId = null
    ) {
        Assert::nullOrUuid($correlationId);

        $this->eventId = Uuid::v4();
        $this->eventType = static::class;
        $this->occurredOn = new CarbonImmutable();
    }

    /**
     * @return array<string, mixed>
     */
    abstract public function payload(): array;
}
