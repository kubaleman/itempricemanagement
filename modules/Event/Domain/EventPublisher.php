<?php declare(strict_types=1);

namespace Modules\Event\Domain;

trait EventPublisher
{
    /**
     * @var array<OutgoingEvent>
     */
    protected array $events = [];

    /**
     * @return array<OutgoingEvent>
     */
    public function getEvents(): array
    {
        $events = $this->events;

        $this->events = [];

        return $events;
    }

    protected function publish(OutgoingEvent $event): void
    {
        $this->events[] = $event;
    }
}
