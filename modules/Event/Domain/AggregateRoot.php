<?php declare(strict_types=1);

namespace Modules\Event\Domain;

interface AggregateRoot
{
    /**
     * @return array<OutgoingEvent>
     */
    public function getEvents(): array;
}
