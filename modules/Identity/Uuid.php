<?php declare(strict_types=1);

namespace Modules\Identity;

use JsonSerializable;
use Stringable;
use Symfony\Component\Uid\UuidV4;

readonly class Uuid implements Stringable, JsonSerializable
{
    private UuidV4 $uuid;

    final public function __construct(?string $uuid = null)
    {
        $this->uuid = null !== $uuid ? UuidV4::fromString($uuid) : new UuidV4();
    }

    public static function fromId(self $uuid): static
    {
        return new static((string) $uuid);
    }

    public function equals(self $comparable): bool
    {
        return $this->uuid->equals($comparable->uuid);
    }

    public function __toString(): string
    {
        return $this->uuid->toRfc4122();
    }

    public function jsonSerialize(): string
    {
        return $this->uuid->toRfc4122();
    }
}
