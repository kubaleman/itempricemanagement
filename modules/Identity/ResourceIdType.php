<?php declare(strict_types=1);

namespace Modules\Identity;

use Source\Shared\Doctrine\Type\UuidDBALType;

final class ResourceIdType extends UuidDBALType
{
    protected function getUuidClassName(): string
    {
        return ResourceId::class;
    }

    public function getName(): string
    {
        return 'resource_id';
    }
}
