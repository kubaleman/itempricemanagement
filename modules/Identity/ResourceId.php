<?php declare(strict_types=1);

namespace Modules\Identity;

final readonly class ResourceId extends Uuid
{
}
