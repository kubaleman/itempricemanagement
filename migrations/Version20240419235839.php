<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240419235839 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE event (event_id UUID NOT NULL, event_type VARCHAR(1024) NOT NULL, occurred_on TIMESTAMP(6) WITHOUT TIME ZONE NOT NULL, correlation_id UUID DEFAULT NULL, created_at TIMESTAMP(6) WITHOUT TIME ZONE NOT NULL, payload_json JSONB NOT NULL, PRIMARY KEY(event_id))');
        $this->addSql('ALTER INDEX uniq_d016d007126f525e RENAME TO UNIQ_E06F3909126F525E');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('DROP TABLE event');
        $this->addSql('ALTER INDEX uniq_e06f3909126f525e RENAME TO uniq_d016d007126f525e');
    }
}
