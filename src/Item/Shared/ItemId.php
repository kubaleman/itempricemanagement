<?php declare(strict_types=1);

namespace Source\Item\Shared;

use Modules\Identity\Uuid;

final readonly class ItemId extends Uuid
{
}
