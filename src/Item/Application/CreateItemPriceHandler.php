<?php declare(strict_types=1);

namespace Source\Item\Application;

use Source\Item\Application\Command\CreateItemPrice;
use Source\Item\Domain\ItemPrice;
use Source\Item\Domain\ItemPriceRepository;
use Source\Item\Shared\ItemId;
use Symfony\Component\Messenger\Attribute\AsMessageHandler;

#[AsMessageHandler]
final readonly class CreateItemPriceHandler
{
    public function __construct(
        private ItemPriceRepository $itemRepository,
    ) {
    }

    public function __invoke(CreateItemPrice $command): void
    {
        $itemId = ItemId::fromId($command->itemId);

        $item = ItemPrice::new($itemId, $command->price);

        $this->itemRepository->save($item);
    }
}
