<?php declare(strict_types=1);

namespace Source\Item\Application\Command;

use Modules\Identity\Uuid;

final readonly class CreateItemPrice
{
    public function __construct(
        public Uuid $itemId,
        /** @var numeric-string */
        public string $price,
    ) {
    }
}
