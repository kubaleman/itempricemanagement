<?php declare(strict_types=1);

namespace Source\Item\Application;

use Source\Item\Application\Command\ChangeItemPrice;
use Source\Item\Application\Exception\ItemNotFoundException;
use Source\Item\Application\Exception\ItemPriceNegativeException;
use Source\Item\Domain\Exception\ItemDomainException;
use Source\Item\Domain\ItemPriceRepository;
use Source\Item\Shared\ItemId;
use Symfony\Component\Messenger\Attribute\AsMessageHandler;

#[AsMessageHandler]
final readonly class ChangeItemPriceHandler
{
    public function __construct(
        private ItemPriceRepository $itemRepository,
    ) {
    }

    /**
     * @throws ItemNotFoundException
     * @throws ItemPriceNegativeException
     */
    public function __invoke(ChangeItemPrice $command): void
    {
        try {
            $price = $this->itemRepository->get(ItemId::fromId($command->itemId));
            $price->changePrice($command->price);
            $this->itemRepository->save($price);
        } catch (ItemDomainException $e) {
            switch ($e->getCode()) {
                case ItemDomainException::DomainBreakCode:
                    throw ItemPriceNegativeException::create();
                case ItemDomainException::NotFoundCode:
                    throw ItemNotFoundException::notFound();
            }
        }
    }
}
