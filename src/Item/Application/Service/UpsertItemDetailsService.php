<?php declare(strict_types=1);

namespace Source\Item\Application\Service;

use Modules\Identity\ResourceId;
use Modules\Identity\Uuid;
use Source\Item\Domain\ItemDetails;
use Source\Item\Domain\ItemDetailsRepository;
use Source\Item\Shared\ItemId;

final readonly class UpsertItemDetailsService
{
    public function __construct(
        private ItemDetailsRepository $detailsRepository
    ) {
    }

    public function create(
        Uuid $itemId,
        string $name,
        string $description
    ): void {
        $itemId = ItemId::fromId($itemId);

        $itemDetails = ItemDetails::new(
            new ResourceId(),
            $itemId,
            $name,
            $description
        );

        $this->detailsRepository->save($itemDetails);
    }

    public function update(
        Uuid $itemId,
        string $name,
        string $description
    ): void {
        $itemId = ItemId::fromId($itemId);

        $itemDetails = $this->detailsRepository->getByItemId($itemId);

        $itemDetails->updateData($name, $description);

        $this->detailsRepository->save($itemDetails);
    }
}
