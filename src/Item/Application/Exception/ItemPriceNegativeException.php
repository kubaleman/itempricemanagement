<?php declare(strict_types=1);

namespace Source\Item\Application\Exception;

use Override;
use Source\Shared\Exception\TranslatedException;

final class ItemPriceNegativeException extends TranslatedException
{
    protected string $name = 'item_price.negative_amount';

    public static function create(): self
    {
        return new self("Item price amount is negative", 3409);
    }

    #[Override]
    public function getName(): string
    {
        return $this->name;
    }
}
