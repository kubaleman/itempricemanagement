<?php declare(strict_types=1);

namespace Source\Item\Application\Exception;

use Override;
use Source\Shared\Exception\TranslatedException;

final class ItemNotFoundException extends TranslatedException
{
    protected string $name = 'item.not_found';

    public static function notFound(): self
    {
        return new self("Item not found", 3404);
    }

    #[Override]
    public function getName(): string
    {
        return $this->name;
    }
}
