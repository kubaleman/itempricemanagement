<?php declare(strict_types=1);

namespace Source\Item\Query;

use Override;
use Source\Shared\Assert\Assert;
use Source\Shared\Query\QueryPagination;

final class ItemPriceQueryParamsPagination extends QueryPagination
{
    /**
     * @param array<string>|null $ids
     * @param array<string>|null $itemIds
     */
    public function __construct(
        public readonly ?array $ids = null,
        public readonly ?array $itemIds = null,
        ?string $sortField = null,
        ?string $sortOrder = null,
        ?int $page = null,
        ?int $perPage = null,
    ) {
        Assert::allUuid($ids ?? []);
        Assert::allUuid($itemIds ?? []);
        parent::__construct($sortField, $sortOrder, $page, $perPage);
    }

    #[Override]
    public function excludedOrderFields(): array
    {
        return ['ids', 'itemIds'];
    }
}
