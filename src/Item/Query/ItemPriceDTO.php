<?php declare(strict_types=1);

namespace Source\Item\Query;

use Modules\Identity\Uuid;
use Money\Money;

final readonly class ItemPriceDTO
{
    public function __construct(
        public Uuid $itemPriceId,
        public Uuid $itemId,
        public Money $price,
    ) {
    }
}
