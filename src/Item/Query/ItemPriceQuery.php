<?php declare(strict_types=1);

namespace Source\Item\Query;

use Modules\Identity\Uuid;
use Source\Shared\Doctrine\Type\ArrayCollection;

interface ItemPriceQuery
{
    public function findById(Uuid $itemPriceId): ?ItemPriceDTO;

    /**
     * @return ArrayCollection<ItemPriceDTO>
     */
    public function get(ItemPriceQueryParamsPagination $queryParams): ArrayCollection;
}
