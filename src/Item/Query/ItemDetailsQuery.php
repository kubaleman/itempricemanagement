<?php declare(strict_types=1);

namespace Source\Item\Query;

use Source\Item\Domain\ItemDetails;

interface ItemDetailsQuery
{
    /**
     * @param null|array<string> $ids
     *
     * @return array<ItemDetails>
     */
    public function findByItemIds(?array $ids): array;
}
