<?php declare(strict_types=1);

namespace Source\Item\Domain;

use Modules\Identity\Uuid;

final readonly class ItemPriceId extends Uuid
{
}
