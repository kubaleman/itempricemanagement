<?php declare(strict_types=1);

namespace Source\Item\Domain;

use Doctrine\ORM\Mapping as ORM;
use Modules\Identity\ResourceId;
use Modules\Identity\Uuid;
use Source\Item\Domain\DTO\ItemDetailsDTO;
use Source\Item\Shared\ItemId;
use Source\Shared\Domain\TimestampsTrait;

#[ORM\Entity]
#[ORM\HasLifecycleCallbacks]
class ItemDetails
{
    use TimestampsTrait;

    public function __construct(
        #[ORM\Id]
        #[ORM\Column(type: 'resource_id', unique: true)]
        private ResourceId $id,
        #[ORM\Column(type: 'item_id', unique: true)]
        private ItemId $itemId,
        #[ORM\Column(length: 255)]
        private string $name,
        #[ORM\Column(length: 1024)]
        private string $description,
    ) {
    }

    public static function new(
        ResourceId $id,
        ItemId $itemId,
        string $name,
        string $description
    ): self {
        return new self($id, $itemId, $name, $description);
    }

    public function updateData(string $name, string $description): void
    {
        $this->name = $name;
        $this->description = $description;
    }

    public function getDetailsDTO(): ItemDetailsDTO
    {
        return new ItemDetailsDTO(
            $this->name,
            $this->description
        );
    }

    public function getId(): Uuid
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getDescription(): string
    {
        return $this->description;
    }

    public function getItemId(): ItemId
    {
        return $this->itemId;
    }
}
