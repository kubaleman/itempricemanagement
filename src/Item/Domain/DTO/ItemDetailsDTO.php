<?php declare(strict_types=1);

namespace Source\Item\Domain\DTO;

final readonly class ItemDetailsDTO
{
    public function __construct(
        public string $name,
        public string $description,
    ) {
    }
}
