<?php declare(strict_types=1);

namespace Source\Item\Domain\Exception;

use Exception;

final class ItemDomainException extends Exception
{
    public const int NotFoundCode = 404;

    public const int DomainBreakCode = 1000;

    public static function notFound(): self
    {
        return new self('item.not_found', self::NotFoundCode);
    }

    public static function negativeAmount(): self
    {
        return new self('item.negative_amount', self::DomainBreakCode);
    }
}
