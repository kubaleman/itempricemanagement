<?php declare(strict_types=1);

namespace Source\Item\Domain;

use Source\Item\Shared\ItemId;

interface ItemDetailsRepository
{
    public function save(ItemDetails $itemDetails): void;

    public function getByItemId(ItemId $itemId): ItemDetails;
}
