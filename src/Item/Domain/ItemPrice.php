<?php declare(strict_types=1);

namespace Source\Item\Domain;

use Doctrine\ORM\Mapping as ORM;
use Modules\Event\Domain\AggregateRoot;
use Modules\Event\Domain\EventPublisher;
use Modules\Identity\Uuid;
use Money\Currency;
use Money\Money;
use Source\Item\Domain\Event\ItemPriceChanged;
use Source\Item\Domain\Event\ItemPriceCreated;
use Source\Item\Domain\Exception\ItemDomainException;
use Source\Item\Shared\ItemId;
use Source\Shared\Domain\TimestampsTrait;

#[ORM\Entity]
#[ORM\HasLifecycleCallbacks]
class ItemPrice implements AggregateRoot
{
    private const string DEFAULT_CURRENCY = "PLN";

    use TimestampsTrait;
    use EventPublisher;

    public function __construct(
        #[ORM\Id]
        #[ORM\Column(type: 'item_price_id', unique: true)]
        private ItemPriceId $id,
        #[ORM\Column(type: 'item_id', unique: true)]
        private ItemId $itemId,
        #[ORM\Embedded(class: Money::class)]
        private Money $price,
    ) {
    }

    /**
     * @throws ItemDomainException
     * @param  numeric-string      $priceAmount
     */
    public static function new(
        ItemId $itemId,
        string $priceAmount
    ): self {
        $price = new Money($priceAmount, new Currency(self::DEFAULT_CURRENCY));

        if ($price->isNegative()) {
            throw ItemDomainException::negativeAmount();
        }

        $itemPriceId = new ItemPriceId();
        $item = new self($itemPriceId, $itemId, $price);

        $item->publish(new ItemPriceCreated($itemPriceId, $itemId, $price));

        return $item;
    }

    /**
     * @param  numeric-string      $priceAmount
     * @throws ItemDomainException
     */
    public function changePrice(string $priceAmount): void
    {
        $price = new Money($priceAmount, $this->price->getCurrency());
        if ($price->isNegative()) {
            throw ItemDomainException::negativeAmount();
        }

        $this->publish(new ItemPriceChanged($this->id, $this->itemId, $price));
        $this->price = $price;
    }

    public function getId(): Uuid
    {
        return $this->id;
    }

    public function getPrice(): Money
    {
        return $this->price;
    }

    public function getItemId(): ItemId
    {
        return $this->itemId;
    }
}
