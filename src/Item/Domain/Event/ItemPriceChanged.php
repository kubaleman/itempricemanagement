<?php declare(strict_types=1);

namespace Source\Item\Domain\Event;

use Money\Money;
use Override;
use Source\Item\Domain\ItemPriceId;
use Source\Item\Shared\ItemId;

final readonly class ItemPriceChanged extends ItemDomainEvent
{
    public function __construct(
        public ItemPriceId $itemPriceId,
        public ItemId $itemId,
        public Money $price
    ) {
        parent::__construct(
            correlationId: $itemPriceId->jsonSerialize(),
        );
    }

    /**
     * @return array{itemId: string, itemPriceId: string, price: array{amount: string, currency: string}}
     */
    #[Override]
    public function payload(): array
    {
        return [
            'itemPriceId' => (string) $this->itemPriceId,
            'itemId' => (string) $this->itemId,
            'price' => $this->price->jsonSerialize(),
        ];
    }
}
