<?php declare(strict_types=1);

namespace Source\Item\Domain\Event;

use Modules\Event\Domain\OutgoingEvent;

abstract readonly class ItemDomainEvent extends OutgoingEvent
{
}
