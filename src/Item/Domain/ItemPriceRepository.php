<?php declare(strict_types=1);

namespace Source\Item\Domain;

use Source\Item\Application\Exception\ItemNotFoundException;
use Source\Item\Shared\ItemId;
use Throwable;

interface ItemPriceRepository
{
    /**
     * @throws Throwable
     */
    public function save(ItemPrice $item): void;

    /**
     * @throws ItemNotFoundException
     */
    public function get(ItemId $itemId): ItemPrice;
}
