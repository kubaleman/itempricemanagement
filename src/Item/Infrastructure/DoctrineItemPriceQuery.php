<?php declare(strict_types=1);

namespace Source\Item\Infrastructure;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;
use Modules\Identity\Uuid;
use Source\Item\Domain\ItemPrice;
use Source\Item\Query\ItemPriceDTO;
use Source\Item\Query\ItemPriceQuery;
use Source\Item\Query\ItemPriceQueryParamsPagination;
use Source\Shared\Doctrine\Type\ArrayCollection;

/**
 * @extends ServiceEntityRepository<ItemPrice>
 */
class DoctrineItemPriceQuery extends ServiceEntityRepository implements ItemPriceQuery
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ItemPrice::class);
    }

    // Queries
    public function findById(Uuid $itemPriceId): ?ItemPriceDTO
    {
        $itemPrice = $this->find($itemPriceId->jsonSerialize());

        return $itemPrice ? new ItemPriceDTO(
            $itemPrice->getId(),
            $itemPrice->getItemId(),
            $itemPrice->getPrice(),
        ) : null;
    }

    /**
     * @return ArrayCollection<ItemPriceDTO>
     */
    public function get(ItemPriceQueryParamsPagination $queryParams): ArrayCollection
    {
        $qb = $this->createQueryBuilder('ip');
        $qb = $this->applyFilters($qb, $queryParams);

        $countingQb = clone $qb;
        $count = $this->countByFilters($countingQb);

        $qb = $this->applyOrder($qb, $queryParams);

        /** @var array<ItemPrice> $itemPrices */
        $itemPrices = $qb->getQuery()->getResult();

        $dtos = array_map(
            static fn (ItemPrice $itemPrice) => new ItemPriceDTO(
                $itemPrice->getId(),
                $itemPrice->getItemId(),
                $itemPrice->getPrice(),
            ),
            $itemPrices
        );

        return new ArrayCollection($dtos, $count);
    }

    private function countByFilters(QueryBuilder $qb): int
    {
        return (int) $qb->select('COUNT(ip.id)')
            ->setFirstResult(null)
            ->setMaxResults(null)
            ->getQuery()
            ->getSingleScalarResult();
    }

    private function applyFilters(QueryBuilder $qb, ItemPriceQueryParamsPagination $queryParams): QueryBuilder
    {
        $queryBuilder = clone $qb;

        if ($queryParams->ids) {
            $queryBuilder->andWhere($queryBuilder->expr()->in('ip.id', ':id'))
                ->setParameter('id', $queryParams->ids);
        }

        if ($queryParams->itemIds) {
            $queryBuilder->andWhere($queryBuilder->expr()->in('ip.itemId', ':itemIds'))
                ->setParameter('itemIds', $queryParams->itemIds);
        }

        return $queryBuilder;
    }

    private function applyOrder(QueryBuilder $qb, ItemPriceQueryParamsPagination $queryParams): QueryBuilder
    {
        $qb->addOrderBy(sprintf('ip.%s', $queryParams->getSortField()), $queryParams->getSortOrder())
            ->setFirstResult($queryParams->calculateOffset())
            ->setMaxResults($queryParams->getPerPage());

        return $qb;
    }
}
