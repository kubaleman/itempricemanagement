<?php declare(strict_types=1);

namespace Source\Item\Infrastructure;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Modules\Event\Domain\EventDispatcher;
use Override;
use Source\Item\Domain\Exception\ItemDomainException;
use Source\Item\Domain\ItemPrice;
use Source\Item\Domain\ItemPriceRepository;
use Source\Item\Shared\ItemId;
use Throwable;

/**
 * @extends ServiceEntityRepository<ItemPrice>
 *
 * @method ItemPrice|null   find($id, $lockMode = null, $lockVersion = null)
 * @method ItemPrice|null   findOneBy(array $criteria, array $orderBy = null)
 * @method array<ItemPrice> findAll()
 * @method array<ItemPrice> findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DoctrineItemPriceRepository extends ServiceEntityRepository implements ItemPriceRepository
{
    public function __construct(
        private EventDispatcher $eventDispatcher,
        ManagerRegistry $registry
    ) {
        parent::__construct($registry, ItemPrice::class);
    }

    /**
     * @throws Throwable
     */
    public function save(ItemPrice $item): void
    {
        $entityManager = $this->getEntityManager();
        $entityManager->beginTransaction();

        try {
            $entityManager->persist($item);
            $this->eventDispatcher->dispatchMany($item->getEvents());

            $entityManager->flush();
            $entityManager->commit();
        } catch (Throwable $exception) {
            $entityManager->rollback();
            throw $exception;
        }
    }

    /**
     * @throws ItemDomainException
     */
    #[Override]
    public function get(ItemId $itemId): ItemPrice
    {
        $item = $this->createQueryBuilder('i')
            ->where('i.itemId = :itemId')
            ->setParameter('itemId', $itemId)
            ->getQuery()
            ->getOneOrNullResult();

        if ($item === null) {
            throw ItemDomainException::notFound();
        }

        /** @var ItemPrice */
        return $item;
    }
}
