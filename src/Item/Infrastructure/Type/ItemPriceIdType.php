<?php declare(strict_types=1);

namespace Source\Item\Infrastructure\Type;

use Source\Item\Domain\ItemPriceId;
use Source\Shared\Doctrine\Type\UuidDBALType;

final class ItemPriceIdType extends UuidDBALType
{
    protected function getUuidClassName(): string
    {
        return ItemPriceId::class;
    }

    public function getName(): string
    {
        return 'item_price_id';
    }
}
