<?php declare(strict_types=1);

namespace Source\Item\Infrastructure\Type;

use Source\Item\Shared\ItemId;
use Source\Shared\Doctrine\Type\UuidDBALType;

final class ItemIdType extends UuidDBALType
{
    protected function getUuidClassName(): string
    {
        return ItemId::class;
    }

    public function getName(): string
    {
        return 'item_id';
    }
}
