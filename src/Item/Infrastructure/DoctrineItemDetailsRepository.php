<?php declare(strict_types=1);

namespace Source\Item\Infrastructure;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Source\Item\Application\Exception\ItemNotFoundException;
use Source\Item\Domain\ItemDetails;
use Source\Item\Domain\ItemDetailsRepository;
use Source\Item\Query\ItemDetailsQuery;
use Source\Item\Shared\ItemId;

/**
 * @extends ServiceEntityRepository<ItemDetails>
 *
 * @method ItemDetails|null   find($id, $lockMode = null, $lockVersion = null)
 * @method ItemDetails|null   findOneBy(array $criteria, array $ItemDetailsBy = null)
 * @method array<ItemDetails> findAll()
 * @method array<ItemDetails> findBy(array $criteria, array $ItemDetailsBy = null, $limit = null, $offset = null)
 */
class DoctrineItemDetailsRepository extends ServiceEntityRepository implements ItemDetailsRepository, ItemDetailsQuery
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ItemDetails::class);
    }

    /**
     * @param array<string> $ids
     *
     * @return array<ItemDetails>
     */
    public function findByItemIds(?array $ids): array
    {
        $qb = $this->createQueryBuilder('i');

        if ($ids !== null) {
            $qb->where('i.itemId IN (:ids)')
                ->setParameter('ids', $ids);
        }

        $result = $qb->getQuery()
            ->getResult();

        /** @var array<ItemDetails> $result */
        return $result;
    }

    public function save(ItemDetails $itemDetails): void
    {
        $this->getEntityManager()->persist($itemDetails);
        $this->getEntityManager()->flush();
    }

    /**
     * @throws ItemNotFoundException
     */
    public function getByItemId(ItemId $itemId): ItemDetails
    {
        $itemDetails = $this->findOneBy([
            'itemId' => $itemId,
        ]);

        if ($itemDetails === null) {
            throw ItemNotFoundException::notFound();
        }

        return $itemDetails;
    }
}
