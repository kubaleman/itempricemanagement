<?php declare(strict_types=1);

namespace Source\Shared\Domain;

use Carbon\CarbonImmutable;
use Doctrine\ORM\Mapping as ORM;

#[ORM\HasLifecycleCallbacks]
trait TimestampsTrait
{
    #[ORM\Column(type: 'datetime_immutable')]
    private CarbonImmutable $createdAt;

    #[ORM\Column(type: 'datetime_immutable')]
    private CarbonImmutable $updatedAt;

    #[ORM\PrePersist]
    #[ORM\PreUpdate]
    public function updatedTimestamps(): void
    {
        $this->updatedAt = new CarbonImmutable('now');
        if ($this->getCreatedAt() === null) {
            $this->createdAt = $this->updatedAt;
        }
    }

    public function getUpdatedAt(): ?CarbonImmutable
    {
        return $this->updatedAt;
    }

    public function getCreatedAt(): ?CarbonImmutable
    {
        return $this->createdAt ?? null;
    }
}
