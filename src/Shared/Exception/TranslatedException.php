<?php declare(strict_types=1);

namespace Source\Shared\Exception;

use Exception;

abstract class TranslatedException extends Exception
{
    protected string $name;

    abstract public function getName(): string;
}
