<?php declare(strict_types=1);

namespace Source\Shared\OpenApi;

use Attribute;
use OpenApi\Attributes\Response;

#[Attribute(Attribute::TARGET_CLASS | Attribute::TARGET_METHOD | Attribute::IS_REPEATABLE)]
final class NoContentResponse extends Response
{
    public function __construct()
    {
        parent::__construct(
            response: 204,
            description: 'No content',
        );
    }
}
