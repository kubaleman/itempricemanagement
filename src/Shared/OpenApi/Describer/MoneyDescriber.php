<?php declare(strict_types=1);

namespace Source\Shared\OpenApi\Describer;

use Money\Money;
use Nelmio\ApiDocBundle\Model\Model;
use Nelmio\ApiDocBundle\ModelDescriber\ModelDescriberInterface;
use OpenApi\Annotations\Schema;
use Symfony\Component\DependencyInjection\Attribute\AutoconfigureTag;
use Symfony\Component\PropertyInfo\Type;

#[AutoconfigureTag('nelmio_api_doc.model_describer', [
    'priority' => 200,
])]
final readonly class MoneyDescriber implements ModelDescriberInterface
{
    public function describe(Model $model, Schema $schema): void
    {
        $schema->type = 'object';
        $schema->example = [
            "amount" => "1410",
            "currency" => "PLN",
        ];
    }

    public function supports(Model $model): bool
    {
        $supportedIdentities = [
            Money::class,
        ];

        return Type::BUILTIN_TYPE_OBJECT === $model->getType()->getBuiltinType() &&
            in_array($model->getType()->getClassName(), $supportedIdentities, true);
    }
}
