<?php declare(strict_types=1);

namespace Source\Shared\OpenApi\Describer;

use Modules\Identity\Uuid;
use Nelmio\ApiDocBundle\Model\Model;
use Nelmio\ApiDocBundle\ModelDescriber\ModelDescriberInterface;
use OpenApi\Annotations\Schema;
use Symfony\Component\DependencyInjection\Attribute\AutoconfigureTag;
use Symfony\Component\PropertyInfo\Type;

#[AutoconfigureTag('nelmio_api_doc.model_describer', [
    'priority' => 200,
])]
final readonly class UuidDescriber implements ModelDescriberInterface
{
    public function describe(Model $model, Schema $schema): void
    {
        $schema->type = 'string';
        $schema->example = '09c328d4-cf30-4fc2-91ff-30599c788ae8';
    }

    public function supports(Model $model): bool
    {
        $supportedIdentities = [
            Uuid::class,
        ];

        return Type::BUILTIN_TYPE_OBJECT === $model->getType()->getBuiltinType() &&
            in_array($model->getType()->getClassName(), $supportedIdentities, true);
    }
}
