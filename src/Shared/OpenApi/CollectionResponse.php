<?php declare(strict_types=1);

namespace Source\Shared\OpenApi;

use Attribute;
use Nelmio\ApiDocBundle\Annotation\Model;
use OpenApi\Attributes\Items;
use OpenApi\Attributes\JsonContent;
use OpenApi\Attributes\Property;
use OpenApi\Attributes\Response;

#[Attribute(Attribute::TARGET_CLASS | Attribute::TARGET_METHOD | Attribute::IS_REPEATABLE)]
final class CollectionResponse extends Response
{
    public function __construct(string $dto)
    {
        parent::__construct(
            response: 200,
            description: 'Items collection',
            content: new JsonContent(
                properties: [
                    new Property(property: 'count', type: 'integer'),
                    new Property(
                        property: 'items',
                        type: 'array',
                        items: new Items(
                            ref: new Model(
                                type: $dto,
                            ),
                        ),
                    ),
                ],
            ),
        );
    }
}
