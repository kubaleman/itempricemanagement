<?php declare(strict_types=1);

namespace Source\Shared\Bus;

use Source\Shared\Exception\TranslatedException;
use Symfony\Component\Messenger\Exception\HandlerFailedException;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Messenger\Stamp\TransportNamesStamp;

final readonly class CommandBus
{
    private const TRANSPORT_ASYNC = 'async';

    private const TRANSPORT_SYNC = 'sync';

    public function __construct(
        private MessageBusInterface $commandBus,
    ) {
    }

    public function async(object $command): void
    {
        $asyncTransportStamp = new TransportNamesStamp([self::TRANSPORT_ASYNC]);

        $stamps = [
            $asyncTransportStamp,
        ];

        $this->commandBus->dispatch($command, $stamps);
    }

    public function sync(object $command): void
    {
        $syncTransportStamp = new TransportNamesStamp([self::TRANSPORT_SYNC]);

        $stamps = [
            $syncTransportStamp,
        ];

        try {
            $this->commandBus->dispatch($command, $stamps);
        } catch (HandlerFailedException $e) {
            $previous = $e->getPrevious();
            if ($previous instanceof TranslatedException) {
                throw $previous;
            }
            throw $e;
        }
    }
}
