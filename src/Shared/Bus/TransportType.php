<?php declare(strict_types=1);

namespace Source\Shared\Bus;

enum TransportType: string
{
    case SYNC = 'sync';
    case ASYNC = 'async';
    case EVENTS = 'events';
}
