<?php declare(strict_types=1);

namespace Source\Shared\Respond;

readonly class Collection
{
    public function __construct(
        /** @var list<PresentableResponse> $items */
        public array $items,
        public int $count,
    ) {
    }
}
