<?php declare(strict_types=1);

namespace Source\Shared\Respond;

use Modules\Identity\Uuid;
use Source\Shared\Exception\TranslatedException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\SerializerInterface;

final readonly class ApiResponder
{
    private const string JSON_FORMAT = 'json';

    public function __construct(
        private SerializerInterface $serializer
    ) {
    }

    public function created(Uuid $createdId): JsonResponse
    {
        $responseData = [
            'id' => $createdId,
        ];

        return new JsonResponse(
            data: $this->serializer->serialize($responseData, self::JSON_FORMAT),
            status: Response::HTTP_CREATED,
            json: true
        );
    }

    public function success(?PresentableResponse $presentable = null): JsonResponse
    {
        if (!$presentable instanceof PresentableResponse) {
            return new JsonResponse();
        }

        return new JsonResponse(
            data: $this->serializer->serialize($presentable, self::JSON_FORMAT),
            json: true,
        );
    }

    public function collection(Collection $collection): JsonResponse
    {
        return new JsonResponse(
            data: $this->serializer->serialize($collection, self::JSON_FORMAT),
            json: true,
        );
    }

    public function notFound(): JsonResponse
    {
        return new JsonResponse(status: Response::HTTP_NOT_FOUND);
    }

    public function noContent(): JsonResponse
    {
        return new JsonResponse(status: Response::HTTP_NO_CONTENT);
    }

    public function updated(Uuid $id): JsonResponse
    {
        return new JsonResponse([
            'id' => $id,
        ], Response::HTTP_OK);
    }

    public function error(TranslatedException $exception): JsonResponse
    {
        return new JsonResponse(
            [
                'name' => $exception->getName(),
                'message' => $exception->getMessage(),
            ],
            Response::HTTP_BAD_REQUEST,
        );
    }

    public function conflict(TranslatedException $exception): JsonResponse
    {
        return new JsonResponse(
            [
                'name' => $exception->getName(),
                'message' => $exception->getMessage(),
            ],
            Response::HTTP_CONFLICT,
        );
    }
}
