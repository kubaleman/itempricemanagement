<?php declare(strict_types=1);

namespace Source\Shared\Resolver;

use Modules\Identity\Uuid;
use Symfony\Component\DependencyInjection\Attribute\AutoconfigureTag;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Controller\ValueResolverInterface;
use Symfony\Component\HttpKernel\ControllerMetadata\ArgumentMetadata;

#[AutoconfigureTag('controller.argument_value_resolver', [
    'priority' => 145,
])]
final readonly class UuidArgumentResolver implements ValueResolverInterface
{
    /**
     * @return list<mixed>
     */
    public function resolve(Request $request, ArgumentMetadata $argument): iterable
    {
        $argumentType = $argument->getType();
        if (!$this->supports($argument)) {
            return [];
        }

        $name = $argument->getName();
        $routeParams = $request->attributes->get('_route_params', []);

        if (!is_array($routeParams) || !array_key_exists($name, $routeParams)) {
            return $argument->isNullable() ? [null] : [];
        }

        if (!is_string($routeParams[$name])) {
            return $argument->isNullable() ? [null] : [];
        }

        /** @var Uuid $argumentType */
        yield new $argumentType($routeParams[$name]);
    }

    private function supports(ArgumentMetadata $argument): bool
    {
        return $argument->getType() === Uuid::class;
    }
}
