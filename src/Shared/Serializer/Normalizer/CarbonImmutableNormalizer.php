<?php declare(strict_types=1);

namespace Source\Shared\Serializer\Normalizer;

use Carbon\CarbonImmutable;
use InvalidArgumentException;
use Source\Shared\Assert\Assert;
use Symfony\Component\DependencyInjection\Attribute\AutoconfigureTag;
use Symfony\Component\Serializer\Exception\NotNormalizableValueException;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

#[AutoconfigureTag('serializer.normalizer')]
final class CarbonImmutableNormalizer implements NormalizerInterface, DenormalizerInterface
{
    /**
     * @param list<mixed> $context
     */
    public function normalize(mixed $object, string $format = null, array $context = []): string
    {
        if (!$this->supportsNormalization($object, $format, $context)) {
            throw new InvalidArgumentException(
                'The object must belong to a CarbonImmutable class',
            );
        }

        Assert::isInstanceOf($object, CarbonImmutable::class);

        return $object->toAtomString();
    }

    /**
     * @param list<mixed> $context
     */
    public function supportsNormalization(mixed $data, string $format = null, array $context = []): bool
    {
        return $data instanceof CarbonImmutable;
    }

    /**
     * @param list<mixed> $context
     */
    public function denormalize(mixed $data, string $type, string $format = null, array $context = []): CarbonImmutable
    {
        if (!$this->supportsDenormalization($data, $type, $format, $context)) {
            throw new InvalidArgumentException(
                'The type must belong to a CarbonImmutable class',
            );
        }

        if (!is_string($data) || '' === $data) {
            throw new NotNormalizableValueException(
                'The data is not a string, you should pass a string that can be parsed as an CarbonImmutable class',
            );
        }

        return (new CarbonImmutable($data))->utc();
    }

    /**
     * @param list<mixed> $context
     */
    public function supportsDenormalization(mixed $data, string $type, string $format = null, array $context = []): bool
    {
        return CarbonImmutable::class === $type;
    }

    /**
     * @return array<class-string, bool>
     */
    public function getSupportedTypes(?string $format): array
    {
        return [
            CarbonImmutable::class => true,
        ];
    }
}
