<?php declare(strict_types=1);

namespace Source\Shared\Serializer\Normalizer;

use Modules\Identity\Uuid;
use Source\Shared\Assert\Assert;
use Source\Shared\Assert\InvalidArgumentException;
use Symfony\Component\DependencyInjection\Attribute\AutoconfigureTag;
use Symfony\Component\Serializer\Exception\NotNormalizableValueException;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

#[AutoconfigureTag('serializer.normalizer')]
final readonly class UuidNormalizer implements NormalizerInterface, DenormalizerInterface
{
    /**
     * @param list<mixed> $context
     */
    public function normalize(mixed $object, string $format = null, array $context = []): string
    {
        if (!$this->supportsNormalization($object, $format, $context)) {
            throw new InvalidArgumentException(
                'The object must belong to a Uuid class',
            );
        }

        Assert::isInstanceOf($object, Uuid::class);

        return (string) $object;
    }

    /**
     * @param list<mixed> $context
     */
    public function supportsNormalization(mixed $data, string $format = null, array $context = []): bool
    {
        return $data instanceof Uuid;
    }

    /**
     * @param list<mixed> $context
     */
    public function denormalize(mixed $data, string $type, string $format = null, array $context = []): Uuid
    {
        if (!$this->supportsDenormalization($data, $type, $format, $context)) {
            throw new InvalidArgumentException(
                'The type must belong to a Uuid class',
            );
        }

        if (!is_string($data)) {
            throw new NotNormalizableValueException(
                'The data is not a string, you should pass a string that can be parsed as an Uuid class',
            );
        }

        /** @var Uuid $className */
        $className = $type;
        return new $className($data);
    }

    /**
     * @param list<mixed> $context
     */
    public function supportsDenormalization(mixed $data, string $type, string $format = null, array $context = []): bool
    {
        return is_a($type, Uuid::class, true);
    }

    /**
     * @return array<'*'|'object'|class-string|string, null|bool>
     */
    public function getSupportedTypes(?string $format): array
    {
        return [
            'object' => null,
            '*' => null,
            Uuid::class => true,
        ];
    }
}
