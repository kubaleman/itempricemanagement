<?php declare(strict_types=1);

namespace Source\Shared\Doctrine\Type;

use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\Type;
use Modules\Identity\Uuid;
use Stringable;
use Symfony\Bridge\Doctrine\Types\UuidType;
use Symfony\Component\Uid\AbstractUid;

abstract class UuidDBALType extends Type
{
    public function getSQLDeclaration(array $column, AbstractPlatform $platform): string
    {
        return $this->getUuidType()->getSQLDeclaration($column, $platform);
    }

    public function convertToDatabaseValue($value, AbstractPlatform $platform): ?string
    {
        $uuidClassName = $this->getUuidClassName();
        if ($value instanceof $uuidClassName && $value instanceof Stringable) {
            $value = (string) $value;
        }

        return $this->getUuidType()->convertToDatabaseValue($value, $platform);
    }

    public function convertToPHPValue($value, AbstractPlatform $platform): ?Uuid
    {
        /** @var Uuid $uuidClassName */
        $uuidClassName = $this->getUuidClassName();
        if ($value instanceof $uuidClassName) {
            return $value;
        }

        $uuid = $this->getUuidType()->convertToPHPValue($value, $platform);

        if (!$uuid instanceof AbstractUid) {
            return null;
        }

        return new $uuidClassName($uuid->toRfc4122());
    }

    private function getUuidType(): UuidType
    {
        static $uuidType = null;

        return $uuidType ??= new UuidType();
    }

    abstract protected function getUuidClassName(): string;

    abstract public function getName(): string;
}
