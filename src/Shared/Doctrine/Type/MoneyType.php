<?php declare(strict_types=1);

namespace Source\Shared\Doctrine\Type;

use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\Type;
use Money\Currency;
use Money\Money;

class MoneyType extends Type
{
    public function getSQLDeclaration(array $column, AbstractPlatform $platform): string
    {
        return $platform->getStringTypeDeclarationSQL($column);
    }

    public function convertToDatabaseValue($value, AbstractPlatform $platform): ?string
    {
        /** @var Money|null $value */
        if ($value === null) {
            return null;
        }

        $moneyData = json_encode([
            'amount' => $value->getAmount(),
            'currency' => $value->getCurrency()->getCode(),
        ]);

        return !$moneyData ? null : $moneyData;
    }

    public function convertToPHPValue($value, AbstractPlatform $platform): ?Money
    {
        /** @var null|string $value */
        if ($value === null) {
            return null;
        }

        /** @var array{
         * amount: int,
         * currency: non-empty-string
         * } $money
         */
        $money = json_decode($value, true);
        return new Money($money['amount'], new Currency($money['currency']));
    }
}
