<?php declare(strict_types=1);

namespace Source\Shared\Doctrine\Type;

use Countable;
use IteratorAggregate;

/**
 * @template TValue
 * @phpstan-extends IteratorAggregate<int, TValue>
 */
interface Collection extends IteratorAggregate, Countable
{
    public const DESC = 'desc';

    public const ASC = 'asc';

    public function total(): int;

    public function count(): int;
}
