<?php declare(strict_types=1);

namespace Source\Shared\Doctrine\Type;

use ArrayObject;
use Traversable;
use function array_values;
use function count;

/**
 * @template TValue
 * @phpstan-implements Collection<TValue>
 */
final class ArrayCollection implements Collection
{
    /**
     * @phpstan-var array<int|string, TValue>
     */
    private array $array;

    private int $total;

    /**
     * @phpstan-param array<int|string, TValue> $array
     */
    public function __construct(array $array, int $total = null)
    {
        $this->array = $array;
        $this->total = $total ?? count($array);
    }

    public function count(): int
    {
        return count($this->array);
    }

    public function total(): int
    {
        return $this->total;
    }

    public function getIterator(): Traversable
    {
        return new ArrayObject(array_values($this->array));
    }
}
