<?php declare(strict_types=1);

namespace Source\Shared\Query;

abstract class QueryPagination
{
    public const SORT_ASC = 'ASC';

    public const SORT_DESC = 'DESC';

    private const DEFAULT_SORT_FIELD = 'createdAt';

    private const DEFAULT_PAGE = 1;

    private const DEFAULT_PER_PAGE = 100;

    private const MAX_PER_PAGE = 500;

    public readonly string $sortField;

    public readonly string $sortOrder;

    private int $page;

    private readonly int $perPage;

    public function __construct(
        ?string $sortField,
        ?string $sortOrder,
        ?int $page,
        ?int $perPage,
    ) {
        if (null !== $sortOrder) {
            $sortOrder = mb_strtoupper($sortOrder);
        }

        if (in_array($sortField, $this->excludedOrderFields(), true)) {
            $sortField = self::DEFAULT_SORT_FIELD;
        }

        $this->sortField = $sortField ?? self::DEFAULT_SORT_FIELD;
        $this->sortOrder = self::SORT_ASC === $sortOrder ? self::SORT_ASC : self::SORT_DESC;
        $this->page = $page ?? self::DEFAULT_PAGE;
        $this->perPage = min($perPage ?? self::DEFAULT_PER_PAGE, self::MAX_PER_PAGE);
    }

    /**
     * @return array<string>
     */
    abstract public function excludedOrderFields(): array;

    public function nextPage(): void
    {
        $this->page++;
    }

    public function calculateOffset(): int
    {
        return $this->perPage * ($this->page - 1);
    }

    public function getSortField(): string
    {
        return $this->sortField;
    }

    public function getSortOrder(): string
    {
        return $this->sortOrder;
    }

    public function getPage(): int
    {
        return $this->page;
    }

    public function getPerPage(): int
    {
        return $this->perPage;
    }
}
