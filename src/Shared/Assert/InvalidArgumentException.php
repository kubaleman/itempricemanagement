<?php declare(strict_types=1);

namespace Source\Shared\Assert;

use Webmozart\Assert\InvalidArgumentException as WebmozartInvalidArgumentExceptionAlias;

final class InvalidArgumentException extends WebmozartInvalidArgumentExceptionAlias
{
    public function getName(): string
    {
        return 'assertion.failed';
    }
}
