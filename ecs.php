<?php declare(strict_types=1);

use PhpCsFixer\Fixer\ClassNotation\ClassDefinitionFixer;
use PhpCsFixer\Fixer\ControlStructure\ElseifFixer;
use PhpCsFixer\Fixer\DoctrineAnnotation\DoctrineAnnotationArrayAssignmentFixer;
use PhpCsFixer\Fixer\FunctionNotation\StaticLambdaFixer;
use PhpCsFixer\Fixer\Import\NoUnusedImportsFixer;
use PhpCsFixer\Fixer\LanguageConstruct\IsNullFixer;
use PhpCsFixer\Fixer\Operator\NotOperatorWithSuccessorSpaceFixer;
use PhpCsFixer\Fixer\Phpdoc\PhpdocAlignFixer;
use PhpCsFixer\Fixer\Phpdoc\PhpdocTrimFixer;
use PhpCsFixer\Fixer\PhpUnit\PhpUnitMethodCasingFixer;
use PhpCsFixerCustomFixers\Fixer\DeclareAfterOpeningTagFixer;
use Symplify\EasyCodingStandard\Config\ECSConfig;
use Symplify\EasyCodingStandard\Exception\Configuration\SuperfluousConfigurationException;

try {
    return ECSConfig::configure()
        ->withPaths([
            __DIR__ . '/app',
            __DIR__ . '/modules',
            __DIR__ . '/src',
            __DIR__ . '/bin',
            __DIR__ . '/public',
            __DIR__ . '/config',
            __DIR__ . '/migrations',
            __DIR__ . '/tests',
            __DIR__ . '/ecs.php',
        ])
        ->withSkip([
            NotOperatorWithSuccessorSpaceFixer::class,
        ])
        ->withFileExtensions(['php'])
        ->withCache(__DIR__ . '/var/cache/ecs')
        ->withPreparedSets(
            psr12: true,
            arrays: true,
            docblocks: true,
            spaces: true,
            strict: true,
            cleanCode: true,
        )
        ->withConfiguredRule(ClassDefinitionFixer::class, [
            'multi_line_extends_each_single_line' => true,
            'space_before_parenthesis' => true,
        ])
        ->withConfiguredRule(PhpUnitMethodCasingFixer::class, [
            'case' => 'snake_case',
        ])
        ->withRules([
            StaticLambdaFixer::class,
            DoctrineAnnotationArrayAssignmentFixer::class,
            NoUnusedImportsFixer::class,
            ElseifFixer::class,
            IsNullFixer::class,
            PhpdocTrimFixer::class,
            PhpdocAlignFixer::class,
            DeclareAfterOpeningTagFixer::class,
        ]);
} catch (SuperfluousConfigurationException) {
}
