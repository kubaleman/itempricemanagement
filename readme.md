### Application's installation

1. In the main directory of the project, run the command `make create`.
2. Run unit tests using the command `make test-unit` to verify if they are functioning correctly.
3. Perform a `GET https://localhost:17443/api/heartbeat` request to check if the application responds properly.
4. Application documentation is available at `https://localhost:17443/api/doc`

#### Noteworthy

| Directory / File         | Description                                                         | 
|--------------------------|---------------------------------------------------------------------|
| `Makefile`               | center of application management, <br/> contains helpfully commands |
| `.docker `               | development container configuration                                 | 
| `docker-compose.dev.yml` | development configuration                                           | 
| `ecs.php`                | code standard fixer rules                                           | 

## Obtaining JWT Token

To obtain a JWT token, you need to send a request to the appropriate endpoint with authentication data.

1. **Login Endpoint**: Send a POST request to the `/login_check` endpoint (or any configured login endpoint) with the appropriate body containing authentication data, such as username and password.

   Example request using `curl`:

    ```bash
    curl -X POST http://localhost:17443/api/login/check -d '{"username":"your_username", "password":"your_password"}'
    ```

2. **Response**: In response, you will receive a JWT token that you can use for authorization in subsequent requests.

## Using JWT Token in Requests

Include the JWT token in the `Authorization` header in your requests to secure endpoints.

```bash
curl -X GET http://localhost:17443/api/items -H "Authorization: Bearer Your_JWT_Token"
